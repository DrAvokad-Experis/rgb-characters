# RGB-Characters


[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Console App in C# for creating RPG Characters

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was created for an assignment to learn more about C#.


## Install

Install Visual Studio. Clone this repository and open with Visual Studio.

## Usage

Go to test project and run tests and/or run application in Visual Studio

## Maintainers

[Alexander Grönberg (@DrAvokad)](https://gitlab.com/DrAvokad)

## Acknowledgements

Thanks to Noroff for providing videos and lectures about C#

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Alexander Grönberg
