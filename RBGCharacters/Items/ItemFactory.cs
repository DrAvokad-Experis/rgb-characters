﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    public class ItemFactory
    {
        /// <summary>
        /// Creates a new weapon with the given name, level requirement, damage, attack speed and weapon type.
        /// </summary>
        /// <param name="name">Name of the weapon</param>
        /// <param name="requiredLevel">Level required to equip the weapon</param>
        /// <param name="damage">Base damage of the weapon</param>
        /// <param name="attackSpeed">Attack speed of the weapon</param>
        /// <param name="type">Type of weapon</param>
        /// <returns>Returns a new weapon with the given properties</returns>
        public static IWeapon CreateWeapon(string name, int requiredLevel, int damage, double attackSpeed, WeaponType type)
        {
            return new Weapon(name, requiredLevel, damage, attackSpeed, type);
        }

        /// <summary>
        /// Creates a new armor with the given name, level requirement, required slot, material and attributes.
        /// </summary>
        /// <param name="name">Name of the armor</param>
        /// <param name="requiredLevel">Level required to equip the armor</param>
        /// <param name="requiredSlot">Slot in which the armor must be equipped</param>
        /// <param name="type">Material of the armor</param>
        /// <param name="attributes">Attributes stored in the armor</param>
        /// <returns></returns>
        public static IArmor CreateArmor(string name, int requiredLevel, Slot requiredSlot, Material type, IAttributes attributes)
        {
            return new Armor(name, requiredLevel, requiredSlot, type, attributes);
        }
    }
}
