﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;


namespace RBGCharacters.Items
{
    public interface IWeapon : IItem
    {
       
        int Damage { get; }
        double AttackSpeed { get; }
        double DPS { get; }
        WeaponType Type { get; }

        public bool Equals(IWeapon other);
    }
}
