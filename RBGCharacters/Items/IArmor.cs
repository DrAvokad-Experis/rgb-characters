﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    public interface IArmor : IItem
    {
        Material Type { get; }
        IAttributes Attributes { get; }
        public bool Equals(IArmor other);
    }

}
