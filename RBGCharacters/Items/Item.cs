﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    internal abstract class Item : IItem
    {
        public string Name { get; }
        public int RequiredLevel { get; }
        public Slot RequiredSlot { get; }
        public Item(string name, int requiredLevel, Slot requiredSlot)
        {
            this.Name = name;
            this.RequiredLevel = requiredLevel;
            this.RequiredSlot = requiredSlot;
        }
    }

    
}
