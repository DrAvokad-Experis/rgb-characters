﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    public interface IItem
    {
        string Name { get; }
        int RequiredLevel { get; }
        Slot RequiredSlot { get; }
    }


}
