﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    internal class Weapon : Item, IWeapon
    {
        public int Damage { get; }
        public double AttackSpeed { get; }
        public double DPS { get; }
        public WeaponType Type { get; }

        internal Weapon(string name, int requiredLevel, int damage, double attackSpeed, WeaponType type) : base(name, requiredLevel, Slot.SLOT_WEAPON)
        {
            this.Damage = damage;
            this.AttackSpeed = attackSpeed;
            this.Type = type;

            DPS = damage * attackSpeed;
        }

        public bool Equals(IWeapon other)
        {
            return Name == other.Name &&
                RequiredLevel == other.RequiredLevel &&
                RequiredSlot == other.RequiredSlot &&
                Damage == other.Damage &&
                AttackSpeed == other.AttackSpeed &&
                DPS == other.DPS &&
                Type == other.Type;

        }
    }
}
