﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Util;

namespace RBGCharacters.Items
{
    internal class Armor : Item, IArmor
    {
        public Material Type { get; }
        public IAttributes Attributes { get; }

        internal Armor(string name, int requiredLevel, Slot requiredSlot, Material type, IAttributes attributes) : base(name, requiredLevel, requiredSlot)
        {
            this.Type = type;
            this.Attributes = attributes;
        }

        public bool Equals(IArmor other)
        {
            return Name == other.Name &&
                RequiredLevel == other.RequiredLevel &&
                RequiredSlot == other.RequiredSlot &&
                Type == other.Type &&
                Attributes.Equals(other.Attributes);
        }
    }
}
