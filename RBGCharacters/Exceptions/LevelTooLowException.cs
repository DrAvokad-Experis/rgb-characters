﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Exceptions
{
    public class LevelTooLowException : Exception
    {
        public LevelTooLowException()
        {
        }

        public LevelTooLowException(string? message) : base(message)
        {
        }

        public override string Message => "Your character's level is to low to equip this item!";
    }
}
