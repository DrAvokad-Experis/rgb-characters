﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Exceptions
{
    public class IncorrectSlotException : Exception
    {
        public IncorrectSlotException()
        {
        }

        public IncorrectSlotException(string? message) : base(message)
        {
        }

        public override string Message => "Your character cannot equip that item in that slot!";
    }
}
