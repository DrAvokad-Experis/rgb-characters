﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Exceptions
{
    public class IncompatibleEquipmentException : Exception
    {
        public IncompatibleEquipmentException()
        {
        }

        public IncompatibleEquipmentException(string? message) : base(message)
        {
        }

        public override string Message => "Your character lack the proficiency to equip that item!";
    }
}
