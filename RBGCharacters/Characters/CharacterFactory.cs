﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Characters
{
    public class CharacterFactory
    {
        /// <summary>
        /// Takes a name as a string and creates a new level 1 delusional with that string as its name property.
        /// </summary>
        /// <param name="name">Name property to give the delusional</param>
        /// <returns>Returns a new level 1 delusional</returns>
        public static ICharacter CreateDelusional(string name)
        {
            return new Delusional(name);
        }

        /// <summary>
        /// Takes a name as a string and creates a new level 1 warrior with that string as its name property.
        /// </summary>
        /// <param name="name">Name property to give the warrior</param>
        /// <returns>Returns a new level 1 warrior</returns>
        public static ICharacter CreateWarrior(string name)
        {
            return new Warrior(name);
        }

        /// <summary>
        /// Takes a name as a string and creates a new level 1 hunter with that string as its name property.
        /// </summary>
        /// <param name="name">Name property to give the hunter</param>
        /// <returns>Returns a new level 1 hunter</returns>
        public static ICharacter CreateHunter(string name)
        {
            return new Hunter(name);
        }

        /// <summary>
        /// Takes a name as a string and creates a new level 1 sneaky boi with that string as its name property.
        /// </summary>
        /// <param name="name">Name property to give the sneaky boi</param>
        /// <returns>Returns a new level 1 sneaky boi</returns>
        public static ICharacter CreateSneakyBoi(string name)
        {
            return new SneakyBoi(name);
        }
    }
}
