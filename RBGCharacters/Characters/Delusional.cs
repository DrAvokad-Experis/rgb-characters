﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Characters
{
    internal class Delusional : Character
    {
        internal Delusional(string name) : base(name, AttributeFactory.CreateDelusionalStartingAttributes(), new Material[] { Material.MATERIAL_CLOTH },
            new WeaponType[] {WeaponType.TYPE_BIG_STICK, WeaponType.TYPE_SMALL_STICK})
        {
            damage = 1;
        }

        /// <summary>
        /// Increases the delusional's level by one and its attributes with
        /// 1+ strength
        /// 1+ dexterity
        /// 5+ intelligence
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            baseStats += AttributeFactory.CreateDelusionalLevelUpAttributes();
        }

        /// <summary>
        /// Takes the total attributes of the delusional and the DPS of any currently equipped weapon and calculates its total damage
        /// </summary>
        internal override void calculateDamage()
        {
            double attributeDamage = 1 + ((double)totalStats.Intelligence / 100);
            try
            {
                if (Inventory[Slot.SLOT_WEAPON] == null)
                {
                    damage = attributeDamage;
                }
                else
                {
                    IWeapon equipedWeapon = (IWeapon)Inventory[Slot.SLOT_WEAPON];
                    damage = equipedWeapon.DPS * attributeDamage;
                }
            }catch (KeyNotFoundException)
            {
                damage = attributeDamage;
            }
        }
    }
}
