﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;
using RBGCharacters.Exceptions;

namespace RBGCharacters.Characters
{
    internal abstract class Character : ICharacter
    {
        public string Name { get; }
        public int Level { get; set; } = 1;
        internal IAttributes baseStats { get; set; }
        internal IAttributes totalStats { get; set; }
        internal double damage { get; set; }
        Material[] acceptableArmorTypes { get; }
        WeaponType[] acceptableWeaponTypes { get; }
        public Dictionary<Slot, IItem> Inventory { get; set; } = new Dictionary<Slot, IItem>();

        internal Character(string name, IAttributes baseStats, Material[] acceptableArmorTypes, WeaponType[] acceptableWeaponTypes)
        {
            this.Name = name;
            this.baseStats = baseStats;
            this.totalStats = baseStats;
            this.acceptableArmorTypes = acceptableArmorTypes;
            this.acceptableWeaponTypes = acceptableWeaponTypes;
        }

        public abstract void LevelUp();

        /// <summary>
        /// Takes an item and a slot to equip the item in. If the item's level, type and material requirement is met
        /// and the item is equiped in the correct slot, the item is equipped. If an item already is equiped in that slot
        /// it is replaced by the new item.
        /// </summary>
        /// <param name="item">The item for the character to equip</param>
        /// <param name="slot">The slot in which to equip the item</param>
        /// <returns></returns>
        /// <exception cref="LevelTooLowException">The item required to equip the item is not met</exception>
        /// <exception cref="IncorrectSlotException">The item is not equipped in the correct slot</exception>
        /// <exception cref="IncompatibleEquipmentException">The character can not equip an item of that type</exception>
        public bool EquipItem(IItem item, Slot slot)
        {
            bool successful = false;

            if (item.RequiredLevel > Level)
            {
                throw new LevelTooLowException();
            } else if (item.RequiredSlot != slot)
            {
                throw new IncorrectSlotException();
            }else

                try
                {
                    IWeapon weapon = (IWeapon)item;
                    if (!acceptableWeaponTypes.Contains(weapon.Type))
                    {
                        throw new IncompatibleEquipmentException();
                    }
                }catch(InvalidCastException)
                {
                    IArmor armor = (IArmor)item;
                    if (!acceptableArmorTypes.Contains(armor.Type))
                    {
                        throw new IncompatibleEquipmentException();
                    }
                }
            {
                if (Inventory.ContainsKey(slot))
                {
                    Inventory.Remove(slot);
                }
                Inventory.Add(slot, item);
                successful = true;
            }


            return successful;
        }

        /// <summary>
        /// Takes the base attributes from the characters level and ads it with all attributes stored in equipped
        /// items and stores the results in the characters total attributes
        /// </summary>
        internal void calculateTotalAttributes()
        {
            totalStats = baseStats;
            foreach (var slot in Inventory)
            {
                try
                {
                    IArmor armor = (IArmor)slot.Value;
                    totalStats += armor.Attributes;
                }
                catch (InvalidCastException)
                {

                }
            }
        }

        /// <summary>
        /// Calculates the characters total attributes from level and equipment and returns it as an IAttributes
        /// </summary>
        /// <returns>The total attributes of the character</returns>
        public IAttributes GetTotalAttributes()
        {
            calculateTotalAttributes();
            return totalStats;
        }

        /// <summary>
        /// Calculates the characters total attributes from level and equipment and then calculates the total damage
        /// generated from those attributes and the currently equipped item if any weapon is equipped.
        /// </summary>
        /// <returns>The total damage of the character</returns>
        public double GetTotalDamage()
        {
            calculateTotalAttributes();
            calculateDamage();
            return damage;
        }


        internal abstract void calculateDamage();
    }
}
