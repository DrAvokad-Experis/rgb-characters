﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Characters
{
    internal class Hunter : Character
    {
        internal Hunter(string name) : base(name, AttributeFactory.CreateHunterStartingAttributes(), new Material[] { Material.MATERIAL_LEATHER, Material.MATERIAL_MAIL },
            new WeaponType[] { WeaponType.TYPE_BOW, WeaponType.TYPE_AXE })
        {
            damage = 1;
        }

        /// <summary>
        /// Increases the hunter's level by one and its attributes with
        /// 1+ strength
        /// 5+ dexterity
        /// 1+ intelligence
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            baseStats += AttributeFactory.CreateHunterLevelUpAttributes();
        }

        /// <summary>
        /// Takes the total attributes of the hunter and the DPS of any currently equipped weapon and calculates its total damage
        /// </summary>
        internal override void calculateDamage()
        {
            double attributeDamage = 1 + ((double)totalStats.Dexterity / 100);
            try
            {
                if (Inventory[Slot.SLOT_WEAPON] == null)
                {
                    damage = attributeDamage;
                }
                else
                {
                    IWeapon equipedWeapon = (IWeapon)Inventory[Slot.SLOT_WEAPON];                    
                    damage = equipedWeapon.DPS * attributeDamage;
                }
            }
            catch (KeyNotFoundException)
            {
                damage = attributeDamage;
            }
        }
    }
}
