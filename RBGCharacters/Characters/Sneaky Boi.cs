﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Characters
{
    internal class SneakyBoi : Character
    {
        internal SneakyBoi(string name) : base(name, AttributeFactory.CreateSneakyBoiStartingAttributes(), new Material[] { Material.MATERIAL_LEATHER },
            new WeaponType[] { WeaponType.TYPE_DAGGER, WeaponType.TYPE_BOW, WeaponType.TYPE_SWORD })
        {
            damage = 1;
        }

        /// <summary>
        /// Increases the sneaky boi's level by one and its attributes with
        /// 1+ strength
        /// 4+ dexterity
        /// 1+ intelligence
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            baseStats += AttributeFactory.CreateSneakyBoiLevelUpAttributes();
        }

        /// <summary>
        /// Takes the total attributes of the sneaky boi and the DPS of any currently equipped weapon and calculates its total damage
        /// </summary>
        internal override void calculateDamage()
        {
            double attributeDamage = 1 + ((double)totalStats.Dexterity / 100);
            try
            {
                if (Inventory[Slot.SLOT_WEAPON] == null)
                {
                    damage = attributeDamage;
                }
                else
                {
                    IWeapon equipedWeapon = (IWeapon)Inventory[Slot.SLOT_WEAPON];
                    damage = equipedWeapon.DPS * attributeDamage;
                }
            }
            catch (KeyNotFoundException)
            {
                damage = attributeDamage;
            }
        }
    }
}