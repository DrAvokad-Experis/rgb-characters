﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Characters
{
    public interface ICharacter
    {
        string Name { get; }
        int Level { get; set; }
        public Dictionary<Slot, IItem> Inventory { get; set; }

        void LevelUp();

        bool EquipItem(IItem item, Slot slot);

        IAttributes GetTotalAttributes();

        double GetTotalDamage();

    }
}
