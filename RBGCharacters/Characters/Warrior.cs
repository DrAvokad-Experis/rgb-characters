﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Characters
{
    internal class Warrior : Character
    {
        internal Warrior(string name) : base(name, AttributeFactory.CreateWarriorStartingAttributes(), new Material[] { Material.MATERIAL_PLATE, Material.MATERIAL_MAIL },
            new WeaponType[] { WeaponType.TYPE_AXE, WeaponType.TYPE_HAMMER, WeaponType.TYPE_BIG_STICK, WeaponType.TYPE_SWORD })
        {
            damage = 1;
        }

        /// <summary>
        /// Increases the warrior's level by one and its attributes with
        /// 3+ strength
        /// 2+ dexterity
        /// 1+ intelligence
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            baseStats += AttributeFactory.CreateWarriorLevelUpAttributes();
        }

        /// <summary>
        /// Takes the total attributes of the warrior and the DPS of any currently equipped weapon and calculates its total damage
        /// </summary>
        internal override void calculateDamage()
        {
            double attributeDamage = 1 + ((double)totalStats.Strength / 100);
            try
            {
                if (Inventory[Slot.SLOT_WEAPON] == null)
                {
                    damage = attributeDamage;
                }
                else
                {
                    IWeapon equipedWeapon = (IWeapon)Inventory[Slot.SLOT_WEAPON];                   
                    damage = equipedWeapon.DPS * attributeDamage;
                }
            }
            catch (KeyNotFoundException)
            {
                damage = attributeDamage;
            }
        }
    }
}