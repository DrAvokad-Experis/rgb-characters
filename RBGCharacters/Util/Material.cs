﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Util
{
    public enum Material
    {
        MATERIAL_CLOTH,
        MATERIAL_LEATHER,
        MATERIAL_MAIL,
        MATERIAL_PLATE
    }
}
