﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Util
{
    public interface IAttributes
    {
        int Strength { get; }
        int Dexterity { get; }
        int Intelligence { get; }

        public static IAttributes operator +(IAttributes a, IAttributes b)
        {
            return new Attributes(a.Strength + b.Strength, a.Dexterity + b.Dexterity, a.Intelligence + b.Intelligence);
        }

        public bool Equals(IAttributes other);
    }
}
