﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Util
{
    internal class Attributes : IAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        internal Attributes(int strength, int dexterity, int intelligence)
        {
            this.Strength = strength;
            this.Dexterity = dexterity;    
            this.Intelligence = intelligence;
        }

        bool IAttributes.Equals(IAttributes other)
        {
            return Strength == other.Strength && Dexterity == other.Dexterity && Intelligence == other.Intelligence;
        }
    }
}
