﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Util
{
    public class AttributeFactory
    {
        /// <summary>
        /// Creates a new IAttributes with the input strength, dexterity and intelligence.
        /// </summary>
        /// <param name="strength">Amount of strength</param>
        /// <param name="dexterity">Amount of dexterity</param>
        /// <param name="intelligence">Amount of intelligence</param>
        /// <returns>Returns a new IAttributes with input strenght, dexterity and intelligence as properties</returns>
        #region BaseStats
        public static IAttributes CreateAttributes(int strength, int dexterity, int intelligence)
        {
            return new Attributes(strength, dexterity, intelligence);
        }
        /// <summary>
        /// Creates the starting attributes for a warrior: 5 strength, 2 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Returns new warrior starting attributes</returns>
        public static IAttributes CreateWarriorStartingAttributes()
        {
            return new Attributes(5, 2, 1);
        }

        /// <summary>
        /// Creates the starting attributes for a sneaky boi: 2 strength, 6 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Returns new sneaky boi starting attributes</returns>
        public static IAttributes CreateSneakyBoiStartingAttributes()
        {
            return new Attributes(2, 6, 1);
        }

        /// <summary>
        /// Creates the starting attributes for a hunter: 1 strength, 7 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Return new hunter starting attributes</returns>
        public static IAttributes CreateHunterStartingAttributes()
        {
            return new Attributes(1, 7, 1);
        }

        /// <summary>
        /// Creates the starting attributes for a delusional: 1 strength, 1 dexterity, 8 intelligence
        /// </summary>
        /// <returns>Returns new delusional starting attributes</returns>
        public static IAttributes CreateDelusionalStartingAttributes()
        {
            return new Attributes(1, 1, 8);
        }
        #endregion

        #region LevelUpStats
        /// <summary>
        /// Creates the level up attributes for a warrior: 3 strength, 2 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Returns new warrior level up attributes</returns>
        public static IAttributes CreateWarriorLevelUpAttributes()
        {
            return new Attributes(3, 2, 1);
        }

        /// <summary>
        /// Creates the level up attributes for a sneaky boi: 1 strength, 4 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Returns new sneaky boi level up attributes</returns>
        public static IAttributes CreateSneakyBoiLevelUpAttributes()
        {
            return new Attributes(1, 4, 1);
        }

        /// <summary>
        /// Creates the level up attributes for a hunter: 1 strength, 5 dexterity, 1 intelligence
        /// </summary>
        /// <returns>Returns new hunter level up attributes</returns>
        public static IAttributes CreateHunterLevelUpAttributes()
        {
            return new Attributes(1, 5, 1);
        }

        /// <summary>
        /// Creates the level up attributes for a delusional: 1 strength, 1 dexterity, 5 intelligence
        /// </summary>
        /// <returns>Returns new delusional level up attributes</returns>
        public static IAttributes CreateDelusionalLevelUpAttributes()
        {
            return new Attributes(1, 1, 5);
        }
        #endregion
    }
}
