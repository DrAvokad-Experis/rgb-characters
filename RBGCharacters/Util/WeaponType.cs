﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBGCharacters.Util
{
    public enum WeaponType
    {
        TYPE_AXE,
        TYPE_BOW,
        TYPE_DAGGER,
        TYPE_HAMMER,
        TYPE_BIG_STICK,
        TYPE_SWORD,
        TYPE_SMALL_STICK
    }
}
