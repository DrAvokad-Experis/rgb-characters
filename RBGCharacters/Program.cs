﻿using RBGCharacters.Util;
using RBGCharacters.Items;
using RBGCharacters.Characters;
using RBGCharacters.Game;

namespace RBGCharacters
{
    public class Program
    {
        public static void Main(string[] args)
        {
            bool running = true;
            string input;
            Console.WriteLine("Welcome to RBG(0,0,0) characters!");
            Console.WriteLine("If you want to quit the game, type <exit>");
            Console.WriteLine("Press enter to continue!");
            while (running)
            {
                input = Console.ReadLine();
                CharacterCreator.CreateCharacter();
                running = false;
            }
            running = true;
            while (running)
            {
                Console.WriteLine("If you want to quit the game, type <exit>");
                Console.WriteLine("");
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("To display your character information type <1>");
                Console.WriteLine("To create a new piece of armor type <2>");
                Console.WriteLine("To create a new weapon type <3>");
                input = Console.ReadLine();
                if (input == "exit")
                {
                    running = false;
                }
                else
                {                                                           
                    switch (input)
                    {
                        case "1":
                            Console.Clear();
                            GameData.GetInstance().LogCharacter();
                            Console.WriteLine("");
                            break;
                        case "2":
                            ArmorCreator.CreateArmor();
                            Console.WriteLine("");
                            break;
                        case "3":
                            WeaponCreator.CreateWeapon();
                            Console.WriteLine("");
                            break;
                    }                   
                }                                
            }
        }

    }
}