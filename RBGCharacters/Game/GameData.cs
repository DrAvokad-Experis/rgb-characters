﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Characters;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Game
{
    internal class GameData
    {
        public ICharacter Character { get; set; }

        public Dictionary<string, IItem> Items { get; set; } = new Dictionary<string, IItem>();

        GameData()
        {

        }

        static GameData _instance;

        public static GameData GetInstance()
        {
            if (_instance == null)
            {
                _instance = new GameData();
            }
            return _instance;
        }

        public void LogArmor(IArmor item)
        {
            Console.WriteLine(item.Name);
            switch (item.Type)
            {
                case Material.MATERIAL_CLOTH:
                    Console.WriteLine("Cloth");
                    break;
                case Material.MATERIAL_LEATHER:
                    Console.WriteLine("Leather");
                    break;
                case Material.MATERIAL_MAIL:
                    Console.WriteLine("Mail");
                    break;
                case Material.MATERIAL_PLATE:
                    Console.WriteLine("Plate");
                    break;
            }
            Console.WriteLine($"Required level: {item.RequiredLevel}");
            switch (item.RequiredSlot)
            {
                case Slot.SLOT_HEAD:
                    Console.WriteLine("Head");
                    break;
                case Slot.SLOT_BODY:
                    Console.WriteLine("Body");
                    break;
                case Slot.SLOT_LEGS:
                    Console.WriteLine("Legs");
                    break;
            }
            Console.WriteLine($"Strength: {item.Attributes.Strength}");
            Console.WriteLine($"Dexterity: {item.Attributes.Dexterity}");
            Console.WriteLine($"Intelligence: {item.Attributes.Intelligence}");
        }

        public void LogWeapon(IWeapon item)
        {
            Console.WriteLine(item.Name);
            switch (item.Type)
            {
                case WeaponType.TYPE_AXE:
                    Console.WriteLine("Axe");
                    break;
                case WeaponType.TYPE_SWORD:
                    Console.WriteLine("Sword");
                    break;
                case WeaponType.TYPE_DAGGER:
                    Console.WriteLine("Dagger");
                    break;
                case WeaponType.TYPE_BOW:
                    Console.WriteLine("Bow");
                    break;
                case WeaponType.TYPE_SMALL_STICK:
                    Console.WriteLine("Small Stick");
                    break;
                case WeaponType.TYPE_BIG_STICK:
                    Console.WriteLine("Big Stick");
                    break;
            }
            Console.WriteLine($"Required level: {item.RequiredLevel}");
            Console.WriteLine("Weapon");
            Console.WriteLine($"Base Damage: {item.Damage}");
            Console.WriteLine($"Attack Speed: {item.AttackSpeed}");
            Console.WriteLine($"DPS: {item.DPS}");
        }

        public void LogCharacter()
        {
            Console.WriteLine(Character.Name);
            string characterClass ="Error";
            try
            {
                Delusional temp = (Delusional)Character;
                characterClass = "Delusional";
            }
            catch (InvalidCastException) { }
            try
            {
                SneakyBoi temp = (SneakyBoi)Character;
                characterClass = "Sneaky Boi";
            }catch (InvalidCastException) { }
            try
            {
                Hunter temp = (Hunter)Character;
                characterClass = "Hunter";
            }catch(InvalidCastException) { }
            try
            {
                Warrior temp = (Warrior)Character;
                characterClass = "Warrior";
            }catch(InvalidCastException){ }
            Console.WriteLine($"Level {Character.Level} {characterClass}");
            Console.WriteLine($"Strength: {Character.GetTotalAttributes().Strength}");
            Console.WriteLine($"Dexterity: {Character.GetTotalAttributes().Dexterity}");
            Console.WriteLine($"Intelligence: {Character.GetTotalAttributes().Intelligence}");
            logEquipment();
        }

        void logEquipment()
        {
            if (Character.Inventory.ContainsKey(Slot.SLOT_HEAD))
            {
                LogArmor((IArmor)Character.Inventory[Slot.SLOT_HEAD]);
            }
            if (Character.Inventory.ContainsKey(Slot.SLOT_BODY))
            {
                LogArmor((IArmor)Character.Inventory[Slot.SLOT_BODY]);
            }
            if(Character.Inventory.ContainsKey(Slot.SLOT_LEGS))
            {
                LogArmor((IArmor)Character.Inventory[Slot.SLOT_LEGS]);
            }
            if(Character.Inventory.ContainsKey(Slot.SLOT_WEAPON))
            {
                LogWeapon((IWeapon)Character.Inventory[Slot.SLOT_WEAPON]);
            }
        }
    }
}
