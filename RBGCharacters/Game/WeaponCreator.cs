﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Game
{
    public class WeaponCreator
    {
        static string name { get; set; }
        static int requiredLevel { get; set; }
        static int damage { get; set; }
        static double attackSpeed { get; set; }
        static WeaponType type { get; set; }

        public static void CreateWeapon()
        {
            bool running = true;
            while (running)
            {
                Console.Clear();
                Console.WriteLine("Here you can create new weapons for your character to equip.");
                getWeaponType();
                Console.Clear();
                getReqLevel();
                Console.Clear();
                getDamage();
                Console.Clear();
                getAttackSpeed();
                Console.Clear();
                getName();
                GameData.GetInstance().Items.Add(name, ItemFactory.CreateWeapon(name,requiredLevel,damage,attackSpeed,type));
                Console.Clear();
                Console.WriteLine("Weapon created!:");
                GameData.GetInstance().LogWeapon((IWeapon)GameData.GetInstance().Items[name]);
                running = false;
            }
        }

        static void getWeaponType()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("To create an axe type <1>");
                Console.WriteLine("To create a sword type <2>");
                Console.WriteLine("To create a dagger type <3>");
                Console.WriteLine("To create a bow type <4>");
                Console.WriteLine("To create a small stick type <5>");
                Console.WriteLine("To create a big stick type <6>");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        type = WeaponType.TYPE_AXE;
                        running = false;
                        break;
                    case "2":
                        type = WeaponType.TYPE_SWORD;
                        running = false;
                        break;
                    case "3":
                        type = WeaponType.TYPE_DAGGER;
                        running = false;
                        break;
                    case "4":
                        type = WeaponType.TYPE_BOW;
                        running = false;
                        break;
                    case "5":
                        type = WeaponType.TYPE_SMALL_STICK;
                        running = false;
                        break;
                    case "6":
                        type = WeaponType.TYPE_BIG_STICK;
                        running = false;
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        static void getReqLevel()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("Type the level requirement you want for the weapon");
                try
                {
                    requiredLevel = int.Parse(Console.ReadLine());
                    running = false;
                }
                catch (FormatException)
                {
                    Console.Clear();
                }
            }
        }

        static void getDamage()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("Type the base damage you want the weapon to have");
                try
                {
                    damage = int.Parse(Console.ReadLine());
                    running = false;
                }
                catch (FormatException)
                {
                    Console.Clear();
                }
            }
        }

        static void getAttackSpeed()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("Type the attack speed you want the weapon to have");
                try
                {
                    attackSpeed = Convert.ToDouble(Console.ReadLine());
                    running = false;
                }
                catch (FormatException)
                {
                    Console.Clear();
                }
            }
        }
        static void getName()
        {

            Console.WriteLine("What do you wish to name your new weapon?");
            name = Console.ReadLine();

        }
    }
}
