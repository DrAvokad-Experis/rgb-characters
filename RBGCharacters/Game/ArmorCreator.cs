﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Items;
using RBGCharacters.Util;

namespace RBGCharacters.Game
{
    public class ArmorCreator
    {
        static string name { get; set; }
        static Slot slot { get; set; }
        static int requiredLevel { get; set; }
        static Material material { get; set; }
        static int strength { get; set; }
        static int dexterity { get; set; }
        static int intelligence { get; set; }

        public static void CreateArmor()
        {
            bool running = true;
            while (running)
            {
                Console.Clear();
                Console.WriteLine("Here you can create new pieces of armor for your character to equip.");
                getSlot();
                Console.Clear();
                getReqLevel();
                Console.Clear();
                getMaterial();
                Console.Clear();
                getStat("strength");
                Console.Clear();
                getStat("dexterity");
                Console.Clear();
                getStat("intelligence");
                Console.Clear();
                getName();
                GameData.GetInstance().Items.Add(name, ItemFactory.CreateArmor(name, requiredLevel, slot, material, AttributeFactory.CreateAttributes(strength,dexterity, intelligence)));
                Console.Clear();
                Console.WriteLine("Armor created!:");
                GameData.GetInstance().LogArmor((IArmor)GameData.GetInstance().Items[name]);
                running = false;
            }
        }

        static void getSlot()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("To create armor for the head type <1>");
                Console.WriteLine("To create armor for the body type <2>");
                Console.WriteLine("To create armor for the legs type <3>");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        slot = Slot.SLOT_HEAD;
                        running = false;
                        break;
                    case "2":
                        slot = Slot.SLOT_BODY;
                        running = false;
                        break;
                    case "3":
                        slot = Slot.SLOT_LEGS;
                        running = false;
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        static void getReqLevel()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("Type the level requirement you want for the armor");
                try
                {
                    requiredLevel = int.Parse(Console.ReadLine());
                    running = false;
                }
                catch (FormatException)
                {
                    Console.Clear();
                }
            }
        }

        static void getMaterial()
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine("Now you need to choose what material the armor should be made of.");
                Console.WriteLine("To create armor made of cloth type <1>");
                Console.WriteLine("To create armor made of leather type <2>");
                Console.WriteLine("To create armor made of mail type <3>");
                Console.WriteLine("To create armor made of plate type <4>");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        material = Material.MATERIAL_CLOTH;
                        running = false;
                        break;
                    case "2":
                        material = Material.MATERIAL_LEATHER;
                        running = false;
                        break;
                    case "3":
                        material = Material.MATERIAL_MAIL;
                        running = false;
                        break;
                    case "4":
                        material = Material.MATERIAL_PLATE;
                        running = false;
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }
        static void getStat(string stat)
        {
            bool running = true;
            while (running)
            {
                Console.WriteLine($"Type the amount of {stat} you wish to have on your armor");
                try
                {
                    int input = int.Parse(Console.ReadLine());
                    switch (stat)
                    {
                        case "strength":
                            strength = input;
                            break;
                        case "dexterity":
                            dexterity = input;
                            break;
                        case "intelligence":
                            intelligence = input;
                            break;
                        default :
                            throw new Exception();
                    }
                    
                    running = false;
                }
                catch (FormatException)
                {
                    Console.Clear ();
                }
            }
        }

        static void getName()
        {
            
             Console.WriteLine("What do you wish to name your new piece of armor?");
             name = Console.ReadLine();
            
        }
        
        
    }
}
