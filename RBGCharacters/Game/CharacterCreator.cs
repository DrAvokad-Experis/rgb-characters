﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RBGCharacters.Characters;

namespace RBGCharacters.Game
{
    public class CharacterCreator
    {
        public static void CreateCharacter()
        {
            Console.Clear();
            bool running = true;
            while (running)
            {
                Console.WriteLine("Time to create your character!");
                Console.WriteLine("Fist you have to choose your class.");
                Console.WriteLine("To play as a warrior type <1>");
                Console.WriteLine("To play as a hunter type <2>");
                Console.WriteLine("To play as a sneaky boi type <3>");
                Console.WriteLine("To play as a delusional type <3000>");
                string input = Console.ReadLine();
                string nameInput;
                switch (input)
                {
                    case "1":
                        Console.WriteLine("Enter your new warrior's name");
                        nameInput = Console.ReadLine();
                        GameData.GetInstance().Character = CharacterFactory.CreateWarrior(nameInput);
                        Console.Clear();
                        Console.WriteLine($"{GameData.GetInstance().Character.Name} the warrior has been created!");
                        running = false;
                        break;
                    case "2":
                        Console.WriteLine("Enter your new hunter's name");
                        nameInput = Console.ReadLine();
                        GameData.GetInstance().Character = CharacterFactory.CreateHunter(nameInput);
                        Console.Clear();
                        Console.WriteLine($"{GameData.GetInstance().Character.Name} the hunter has been created!");
                        running = false;
                        break;
                    case "3":
                        Console.WriteLine("Enter your new sneaky boi's name");
                        nameInput = Console.ReadLine();
                        GameData.GetInstance().Character = CharacterFactory.CreateSneakyBoi(nameInput);
                        Console.Clear();
                        Console.WriteLine($"{GameData.GetInstance().Character.Name} the sneaky boi has been created!");
                        running = false;
                        break;
                    case "3000":
                        Console.WriteLine("Enter your new delsuional's name");
                        nameInput = Console.ReadLine();
                        GameData.GetInstance().Character = CharacterFactory.CreateDelusional(nameInput);
                        Console.Clear();
                        Console.WriteLine($"{GameData.GetInstance().Character.Name} the delusional has been created!");
                        running = false;
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }
    }
}
