﻿using Xunit;
using RBGCharacters.Util;
using RBGCharacters.Items;

namespace RBGCharactersTesting
{
    public class RBGArmorTest
    {
        [Fact]
        public void CreateArmor_CreateArmorAndCheckName_ShouldReturnValidString()
        {
            // Arrange
            IItem testArmor = ItemFactory.CreateArmor("Blue Bathrobe", 3, Slot.SLOT_BODY, Material.MATERIAL_CLOTH, AttributeFactory.CreateAttributes(0, 3, 3));
            string expected = "Blue Bathrobe";
            string actual = testArmor.Name;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IArmorEquals_CreateArmorAndTestEquals_ShouldReturnTrue()
        {
            // Arrange
            IArmor testArmor1 = ItemFactory.CreateArmor("Chainmail Tank-top", 6, Slot.SLOT_BODY, Material.MATERIAL_MAIL, AttributeFactory.CreateAttributes(6, 2, 1));
            IArmor testArmor2 = ItemFactory.CreateArmor("Chainmail Tank-top", 6, Slot.SLOT_BODY, Material.MATERIAL_MAIL, AttributeFactory.CreateAttributes(6, 2, 1));
            bool expected = true;
            // Act
            bool actual = testArmor1.Equals(testArmor2);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IArmorEquals_CreateArmorAndTestNotEquals_ShouldReturnFalse()
        {
            // Arrange
            IArmor testArmor1 = ItemFactory.CreateArmor("Chainmail Tank-top", 6, Slot.SLOT_BODY, Material.MATERIAL_MAIL, AttributeFactory.CreateAttributes(6, 2, 1));
            IArmor testArmor2 = ItemFactory.CreateArmor("Lederhosen", 5, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 5, 1));
            bool expected = false;
            // Act
            bool actual = testArmor1.Equals(testArmor2);
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}