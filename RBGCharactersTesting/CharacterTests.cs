﻿using Xunit;
using RBGCharacters.Util;
using RBGCharacters.Items;
using RBGCharacters.Characters;
using RBGCharacters.Exceptions;

namespace RBGCharactersTesting
{
    public class CharacterTests
    {
        #region Creation
        [Fact]
        public static void CreateDelusional_createDelusionalAndCheckName_ShouldReturnValidString()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            string expected = "John MacAfee";
            string actual = testCharacter.Name;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public static void CreateDelusional_createDelusionalWithNoWeaponAndCheckDamage_ShouldReturnValidDouble()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            double expected = 1.08;
            // Act
            double actual = testCharacter.GetTotalDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateDelusional_createDelusionalAndCheckLevel_ShouldReturnValidInt()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            int expected = 1;
            int actual = testCharacter.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateWarrior_createWarriorAndCheckStartingAttributes_ShoudlReturnValidAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IAttributes expected = AttributeFactory.CreateAttributes(5, 2, 1);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public static void CreateSneakyBoi_createSneakyBoiAndCheckStartingAttributes_ShoudlReturnValidAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IAttributes expected = AttributeFactory.CreateAttributes(2, 6, 1);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public static void CreateHunter_createHunterAndCheckStartingAttributes_ShoudlReturnValidAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateHunter("Colonel Sanders");
            IAttributes expected = AttributeFactory.CreateAttributes(1, 7, 1);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public static void CreateDelusional_createDelusionalAndCheckStartingAttributes_ShoudlReturnValidAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            IAttributes expected = AttributeFactory.CreateAttributes(1, 1, 8);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }
        #endregion

        #region Level Up
        [Fact]
        public static void LevelUp_createWarriorAndLevelUp_ShouldHaveIncreasedLevelByOne()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            int expected = 2;
            // Act
            testCharacter.LevelUp();
            int actual = testCharacter.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void LevelUp_createHunterAndLevelUp_ShouldHaveIncreasedAttributesByCorrectAmount()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateHunter("Colonel Sanders");
            IAttributes expectedStats = AttributeFactory.CreateAttributes(2, 12, 2);
            bool expected = true;
            // Act
            testCharacter.LevelUp();
            IAttributes actualStats = testCharacter.GetTotalAttributes();
            bool actual = expectedStats.Equals(actualStats);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_createWarriorAndLevelUp_ShouldHaveIncreasedAttributesByCorrectAmount()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IAttributes expected = AttributeFactory.CreateAttributes(8, 4, 2);
            // Act
            testCharacter.LevelUp();
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public void LevelUp_createSneakyBoiAndLevelUp_ShouldHaveIncreasedAttributesByCorrectAmount()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IAttributes expected = AttributeFactory.CreateAttributes(3, 10, 2);
            // Act
            testCharacter.LevelUp();
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public void LevelUp_createDelusionalAndLevelUp_ShouldHaveIncreasedAttributesByCorrectAmount()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            IAttributes expected = AttributeFactory.CreateAttributes(2, 2, 13);
            // Act
            testCharacter.LevelUp();
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }
        #endregion

        #region Items - Weapons
        [Fact]
        public void EquipItem_createWarriorAndEquipValidItem_ShouldReturnTrue()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IWeapon testWeapon = ItemFactory.CreateWeapon("Oak branch", 1, 2, 1, WeaponType.TYPE_BIG_STICK);
            // Act & Assert
            Assert.True(testCharacter.EquipItem(testWeapon, Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquipItem_createWarriorAndEquipItemInNonEmptySlot_ShouldReturnMostRecentlyEquipedItem()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IWeapon testWeapon1 = ItemFactory.CreateWeapon("Oak branch", 1, 2, 2, WeaponType.TYPE_BIG_STICK);
            IWeapon testWeapon2 = ItemFactory.CreateWeapon("Pa's Trusty Ol' Axe", 1, 5, 1.5, WeaponType.TYPE_AXE);
            IWeapon expectedWeapon = ItemFactory.CreateWeapon("Pa's Trusty Ol' Axe", 1, 5, 1.5, WeaponType.TYPE_AXE);
            // Act
            testCharacter.EquipItem(testWeapon1, Slot.SLOT_WEAPON);
            testCharacter.EquipItem(testWeapon2, Slot.SLOT_WEAPON);
            // Assert
            Assert.True(expectedWeapon.Equals((IWeapon)testCharacter.Inventory[Slot.SLOT_WEAPON]));
        }

        [Fact]
        public void EquipItem_createWarriorAndEquipItemInInvalidSlot_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IWeapon testWeapon = ItemFactory.CreateWeapon("Oak branch", 1, 2, 2, WeaponType.TYPE_BIG_STICK);
            // Act & Arrange
            Assert.Throws<IncorrectSlotException>(() => testCharacter.EquipItem(testWeapon, Slot.SLOT_HEAD));
        }

        [Fact]
        public void EquipItem_createWarriorAndEquipItemLackingRequiredLevel_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IItem testWeapon = ItemFactory.CreateWeapon("Girthy Log", 10, 100, 0.2, WeaponType.TYPE_BIG_STICK);
            // Act & Arrange
            Assert.Throws<LevelTooLowException>(() => testCharacter.EquipItem(testWeapon, Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquipItem_createWarriorAndEquipItemLackingRequiredProficiency_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateWarrior("Lill-Snorre");
            IItem testWeapon = ItemFactory.CreateWeapon(" <-(|-= ", 1, 20, 1, WeaponType.TYPE_BOW);
            // Act & Arrange
            Assert.Throws<IncompatibleEquipmentException>(() => testCharacter.EquipItem(testWeapon,Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquipItem_createDelusionalAndEquipWeapon_ShouldReturnIncreasedDamage()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            IWeapon testWeapon = ItemFactory.CreateWeapon("Whittled stick", 1, 10, 1, WeaponType.TYPE_SMALL_STICK);
            double expected = 10.8;
            // Act
            testCharacter.EquipItem(testWeapon, Slot.SLOT_WEAPON);
            double actual = testCharacter.GetTotalDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region Items - Armor
        [Fact]
        public void EquipItem_createSneakyBoiAndEquipValidArmor_ShouldReturnTrue()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor = ItemFactory.CreateArmor("Lederhosen", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 2, 2));
            // Act & Assert
            Assert.True(testCharacter.EquipItem(testArmor,Slot.SLOT_LEGS));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipArmorInNonEmptySlot_ShouldReturnMostRecentlyEqippedArmor()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor1 = ItemFactory.CreateArmor("Lederhosen", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 2, 2));
            IArmor testArmor2 = ItemFactory.CreateArmor("The Tightness", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(3, -1, 2));
            IArmor expectedArmor = ItemFactory.CreateArmor("The Tightness", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(3, -1, 2));
            // Act
            testCharacter.EquipItem(testArmor1, Slot.SLOT_LEGS);
            testCharacter.EquipItem(testArmor2, Slot.SLOT_LEGS);
            // Assert
            Assert.True(expectedArmor.Equals((IArmor)testCharacter.Inventory[Slot.SLOT_LEGS]));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipArmorInInvalidSlot_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor = ItemFactory.CreateArmor("Lederhosen", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 2, 2));
            // Act & Assert
            Assert.Throws<IncorrectSlotException>(() => testCharacter.EquipItem(testArmor, Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipArmorLackingRequiredLevel_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor = ItemFactory.CreateArmor("Whale-leather Trousers", 10, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(7, 2, 4));
            // Act & Assert
            Assert.Throws<LevelTooLowException>(() => testCharacter.EquipItem(testArmor, Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipArmorLackingRequiredProficiency_ShouldThrowCorrectException()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor = ItemFactory.CreateArmor("Chainmail Tank-top", 1, Slot.SLOT_BODY, Material.MATERIAL_MAIL, AttributeFactory.CreateAttributes(4, 2, 2));
            // Act & Assert
            Assert.Throws<IncompatibleEquipmentException>(() => testCharacter.EquipItem(testArmor, Slot.SLOT_BODY));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipValidArmor_ShouldReturnIncreasedAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor = ItemFactory.CreateArmor("Lederhosen", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 2, 2));
            IAttributes expected = AttributeFactory.CreateAttributes(4, 8, 3);
            // Act
            testCharacter.EquipItem(testArmor, Slot.SLOT_LEGS);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public void EquipItem_createSneakyBoiAndEquipTwoValidArmors_ShouldReturnIncreasedAttributes()
        {
            // Arrange
            ICharacter testCharacter = CharacterFactory.CreateSneakyBoi("Shkreli");
            IArmor testArmor1 = ItemFactory.CreateArmor("Lederhosen", 1, Slot.SLOT_LEGS, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(2, 2, 2));
            IArmor testArmor2 = ItemFactory.CreateArmor("Old-school Pilot Helmet", 1, Slot.SLOT_HEAD, Material.MATERIAL_LEATHER, AttributeFactory.CreateAttributes(1, 3, 4));
            IAttributes expected = AttributeFactory.CreateAttributes(5, 11, 7);
            // Act
            testCharacter.EquipItem(testArmor1, Slot.SLOT_LEGS);
            testCharacter.EquipItem(testArmor2, Slot.SLOT_HEAD);
            // Assert
            Assert.True(expected.Equals(testCharacter.GetTotalAttributes()));
        }

        [Fact]
        public void GetTotalDamage_createDelusinalAndTestDamageFullyEquiped_ShouldReturnIncreasedDamage()
        {
            //Arrange
            ICharacter testCharacter = CharacterFactory.CreateDelusional("John MacAfee");
            IArmor testHead = ItemFactory.CreateArmor("Aviators", 1, Slot.SLOT_HEAD, Material.MATERIAL_CLOTH, AttributeFactory.CreateAttributes(1, 1, 4));
            IArmor testBody = ItemFactory.CreateArmor("Red Bathrobe", 1, Slot.SLOT_BODY, Material.MATERIAL_CLOTH, AttributeFactory.CreateAttributes(1, 2, 4));
            IArmor testLegs = ItemFactory.CreateArmor("Silk Pants", 1, Slot.SLOT_LEGS, Material.MATERIAL_CLOTH, AttributeFactory.CreateAttributes(1, 3, 2));
            IWeapon testWeapon = ItemFactory.CreateWeapon("Fancy Cane", 1, 15, 2, WeaponType.TYPE_BIG_STICK);
            double expected = 35.4;
            // Act
            testCharacter.EquipItem(testHead, Slot.SLOT_HEAD);
            testCharacter.EquipItem(testBody, Slot.SLOT_BODY);
            testCharacter.EquipItem(testLegs, Slot.SLOT_LEGS);
            testCharacter.EquipItem(testWeapon, Slot.SLOT_WEAPON);
            double actual = testCharacter.GetTotalDamage();
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
