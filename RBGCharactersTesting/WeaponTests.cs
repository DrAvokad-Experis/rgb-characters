﻿using Xunit;
using RBGCharacters.Util;
using RBGCharacters.Items;

namespace RBGCharactersTesting
{
    public class WeaponTests
    {
        [Fact]
        public void weapon_CreateWeaponAndCheckName_ShouldReturnValidString()
        {
            // Arrange
            IItem testWeapon = ItemFactory.CreateWeapon("Girthy Log", 10, 100, 0.2, WeaponType.TYPE_BIG_STICK);
            string expected = "Girthy Log";
            string actual = testWeapon.Name;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void weapon_CreateWeaponAndCheckEquals_ShouldReturnTrue()
        {
            // Arrange
            IWeapon testWeapon1 = ItemFactory.CreateWeapon("Rolling Pin", 1, 4, 4, WeaponType.TYPE_HAMMER);
            IWeapon testWeapon2 = ItemFactory.CreateWeapon("Rolling Pin", 1, 4, 4, WeaponType.TYPE_HAMMER);
            bool expected = true;
            // Act
            bool actual = testWeapon1.Equals(testWeapon2);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void weapon_CreateWeaponAndCheckNotEquals_ShouldReturnTrue()
        {
            // Arrange
            IWeapon testWeapon1 = ItemFactory.CreateWeapon("Rolling Pin", 1, 4, 4, WeaponType.TYPE_HAMMER);
            IWeapon testWeapon2 = ItemFactory.CreateWeapon("Purple Lightsaber", 12, 65, 3, WeaponType.TYPE_SWORD);
            bool expected = false;
            // Act
            bool actual = testWeapon1.Equals(testWeapon2);
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
