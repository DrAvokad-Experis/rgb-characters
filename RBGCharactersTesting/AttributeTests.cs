using Xunit;
using RBGCharacters.Util;

namespace RBGCharactersTesting
{
    public class AttributeTests
    {
        [Fact]
        public void CreateWarriorStartingAttributes_CreateWarriorStartingAttributesAndCheckStrength_ShouldReturnValidStartingValue()
        {
            // Arrange
            int expected = 5;
            IAttributes warriorAttributes = AttributeFactory.CreateWarriorStartingAttributes();
            int actual = warriorAttributes.Strength;
            // Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void IAttributesAddOperator_AddAttributesToWarriorStartingAttributes_ShouldReturnNewIncreasedAttributes()
        {
            // Arrange
            bool expected = true;
            IAttributes warriorAttribues = AttributeFactory.CreateWarriorStartingAttributes();
            IAttributes attributesToAdd = AttributeFactory.CreateAttributes(1, 1, 1);
            IAttributes totalAttributes = AttributeFactory.CreateAttributes(6, 3, 2);
            // Act
            bool actual = totalAttributes.Equals(warriorAttribues + attributesToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IAttributesEquals_AddAttributesToWarriorStartingAttributesAndTestEquals_ShouldReturnNewIncreasedAttributes()
        {
            // Arrange
            bool expected = false;
            IAttributes warriorAttribues = AttributeFactory.CreateWarriorStartingAttributes();
            IAttributes attributesToAdd = AttributeFactory.CreateAttributes(1, 10, 1);
            IAttributes totalAttributes = AttributeFactory.CreateAttributes(6, 3, 2);
            // Act
            bool actual = totalAttributes.Equals(warriorAttribues + attributesToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }
    }

}